from flask import Flask, render_template, url_for, request, redirect, current_app
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from wiki_utils import *
from db_utils import *
from export_utils import *
# todo implement proper error handling via https://flask-restplus.readthedocs.io/en/stable/errors.html
# todo add README file

app = Flask(__name__)
db_name = "ref_archive.db"
app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{db_name}'
app.config['CLIENT_TXT'] = EXPORT_PATH
db = SQLAlchemy(app)

# todo move to db_utils
class WikiPage(db.Model):
    page_title = db.Column(db.String, primary_key=True)
    page_wikitext = db.Column(db.String)
    last_revision_id = db.Column(db.String)
    date_added = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        title_or_url = request.form.get('title_url')
        wikitext_portion = request.form.get('wikitxt_portion')
        checkbox_full = request.form.get('live_included_0')
        checkbox_portion = request.form.get('live_included_1')

        if title_or_url:

            wiki_page_title = url_to_title(title_or_url)
            wiki_response = wiki_page_getter(wiki_page_title)

            if wiki_response.startswith("Wrong") or wiki_response.startswith("Connection"):
                return render_template("index.html", error=wiki_response)

            else:
                rev_id = revision_id_getter(wiki_page_title)

                if record_validator(db, WikiPage, rev_id):
                    try:
                        archived_wikitext = archived_text_getter(db, WikiPage, wiki_page_title)
                        file_name = txt_writer(archived_wikitext, page_name=wiki_page_title)

                        return redirect(f"/txt_export/{file_name}")

                    except Exception as e:
                        return render_template("index.html", error=f"{e}")

                else:
                    try:
                        delete_outdated_record(db, WikiPage, wiki_page_title)
                    except Exception as e:
                        return render_template("index.html", error=f"{e}")

                    if checkbox_full == "True":
                        processed_page = wikitext_processor(wiki_response)

                        page_record = WikiPage(
                            page_title=wiki_page_title,
                            page_wikitext=processed_page,
                            last_revision_id=rev_id
                        )
                    else:
                        processed_page = wikitext_processor(wiki_response, include_live_urls=False)

                        page_record = WikiPage(
                            page_title=wiki_page_title,
                            page_wikitext=processed_page,
                            last_revision_id=rev_id
                        )
                    try:
                        db.session.add(page_record)
                        db.session.commit()
                    except Exception as e:
                        return render_template("index.html", error=f"{e}")

                    try:
                        file_name = txt_writer(processed_page, page_name=wiki_page_title)
                    except Exception as e:
                        return render_template("index.html", error=f"{e}")

                    return redirect(f"/txt_export/{file_name}")

        elif wikitext_portion:
            if checkbox_portion == "True":
                processed_portion = trim_excessive_new_line(wikitext_processor(wikitext_portion))
            else:
                processed_portion = trim_excessive_new_line(wikitext_processor(wikitext_portion, include_live_urls=False))

            try:
                file_name = txt_writer(processed_portion)
            except Exception as e:
                return render_template("index.html", error=f"{e}")

            return redirect(f"/txt_export/{file_name}")
        else:
            return render_template("index.html", error="Empty field!")
    else:
        return render_template("index.html")


@app.route('/txt_export/<txt_name>')
def export_as_txt(txt_name):
    try:
        file_path = os.path.join(get_path(), txt_name)
        file_handle = open(file_path, 'r', encoding='utf-8')

        def stream_and_remove():
            yield from file_handle
            file_handle.close()
            os.remove(file_path)

        return current_app.response_class(
            stream_and_remove(),
            headers=
            {
                'Content-Disposition': 'attachment', 'filename': txt_name, 'Content-Type': 'text; charset=utf-8'
            }
        )
    except Exception as e:
        return render_template("index.html", error=f"{e}")


if __name__ == "__main__":
    app.run(debug=True)

import os
from datetime import datetime

# todo clear default path variable
EXPORT_PATH = r"C:\Users\1\Desktop\flask_wikiarchive_project\static\client\txt"


def txt_writer(processed_wikitext, path=EXPORT_PATH, page_name=None):
    tail = f"-{str(datetime.now())}"[:-7].replace(":", "-")

    if page_name:
        file_name = page_name + tail + ".txt"
        full_path = os.path.join(path, file_name)

        with open(full_path, "w", encoding="utf-8") as export_txt:
            export_txt.writelines(processed_wikitext)
    else:
        file_name = "portion" + tail + ".txt"
        full_path = os.path.join(path, file_name)

        with open(full_path, "w", encoding="utf-8") as export_txt:
            export_txt.writelines(processed_wikitext)

    return file_name


def get_path(default=EXPORT_PATH):
    return default

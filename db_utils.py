def record_validator(db, db_model, current_revision_id):
    # if unique revision_id is not in the database
    if db.session.query(db_model.last_revision_id).filter_by(last_revision_id=f'{current_revision_id}').scalar() is not None:
        return True   # the page version requested for archiving is already archived
    else:
        return False  # not archived


def delete_outdated_record(db, db_model, page_title):
    db.session.query(db_model.page_title).filter_by(page_title=page_title).delete()
    db.session.commit()


def archived_text_getter(db, db_model, page_title):
    return db.session.query(db_model.page_wikitext).filter_by(page_title=f'{page_title}').scalar()

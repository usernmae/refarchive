import requests
import re
from wayback_utils import web_archive_getter


wiki_session = requests.Session()
status_checker_session = requests.Session()
wiki_api_url = "https://en.wikipedia.org/w/api.php"


def url_to_title(wiki_page_identifier):
    if not wiki_page_identifier.startswith("https://"):
        return wiki_page_identifier
    else:
        return wiki_page_identifier.split("/")[-1].replace("_", " ")


def wiki_page_getter(page_title, wiki_session=wiki_session):
    GET_WIKITEXT_PARAMS = {
        "action": "parse",
        "page": page_title,
        "format": "json",
        "prop": "wikitext"
    }

    try:
        wiki_response = wiki_session.get(url=wiki_api_url, params=GET_WIKITEXT_PARAMS)
        wiki_data = wiki_response.json()
        original_wikitext = wiki_data.get("parse").get("wikitext").get("*")
        return original_wikitext
    except AttributeError:
        return "Wrong page title or URL."
    except ConnectionError:
        return "Connection to Wikipedia failed."


def revision_id_getter(page_title, wiki_session=wiki_session):
    GET_REVISION_ID_PARAMS = {
        "action": "parse",
        "page": page_title,
        "format": "json",
        "prop": "revid"
    }
    try:
        wiki_response = wiki_session.get(url=wiki_api_url, params=GET_REVISION_ID_PARAMS)
        wiki_data = wiki_response.json()
        revision_id = wiki_data.get("parse").get("revid")
        return revision_id
    except AttributeError:
        return "Wrong page title or URL."
    except ConnectionError:
        return "Connection to Wikipedia failed."


def ref_tail_constructor(archive_url, archive_date, arch_url_status):
    if int(arch_url_status) in range(400):  # redirects are filtered by webarchive
        url_status = "live"
    else:
        url_status = "dead"

    return f" |archive-url={archive_url} |archive-date={archive_date} |url-status={url_status} "


def archive_link_checker(ref_body_with_ending):
    filter_out = [
        "web.archive", "archive.today", "archive.is", "archive.li",
        "archive.fo", "archive.ph", "archive.vn", "archive.md"
    ]

    for item in filter_out:
        if item in ref_body_with_ending:
            return False

    return True  # clear to proceed


def wikitext_processor(wikitext, include_live_urls=True):
    split_by_ref = re.split('(\<ref)', wikitext)

    for index, item in enumerate(split_by_ref):
        if item == "<ref":
            ref_body_index = index + 1
            ref_body_with_ending = split_by_ref[ref_body_index]
            try:
                if archive_link_checker(ref_body_with_ending):
                    ref_body_without_ending = ref_body_with_ending.split("}}")  # lies at index [0]

                    if len(ref_body_without_ending) != 1:  # for the case when there is a simple <ref></ref> construct without the {{content}} part
                        url_to_archive = re.findall("(?:htt)+(?:.*?)\|", ref_body_without_ending[0])[0][:-1]  # cutting the pipe delimiter
                        if url_to_archive:
                            w_a_response = web_archive_getter(url_to_archive)
                            if w_a_response:
                                if int(w_a_response[2]) in range(400) and not include_live_urls:
                                    # filtering live urls
                                    continue
                                elif int(w_a_response[2]) in range(400) and include_live_urls:
                                    # archiving live urls
                                    print(f"Archived {url_to_archive}")
                                    ref_body_without_ending[0] += ref_tail_constructor(
                                        w_a_response[0], w_a_response[1], w_a_response[2]
                                    )
                                else:
                                    # archiving dead urls
                                    print(f"!ARCHIVED DEAD! {url_to_archive}")
                                    ref_body_without_ending[0] += ref_tail_constructor(
                                        w_a_response[0], w_a_response[1], w_a_response[2]
                                    )
                                split_by_ref[ref_body_index] = "}}".join(ref_body_without_ending)
            except:
                print("Wikitext processor issue.")
    return "".join(split_by_ref)


def trim_excessive_new_line(processed_wikitext_portion):
    n_line_split = processed_wikitext_portion.split("\n")

    for index, item in enumerate(n_line_split):
        if item == "\r":
            del n_line_split[index]

    return "".join(n_line_split)

import requests


web_archive_session = requests.Session()
web_archive_api_url = "http://archive.org/wayback/available?url="


def web_archive_getter(url, w_a_session=web_archive_session, api_url=web_archive_api_url):
    months = {"01": "January", "02": "February", "03": "March", "04": "April", "05": "May", "06": "June", "07": "July",
              "08": "August", "09": "September", "10": "October", "11": "November", "12": "December"}
    try:
        w_a_response = w_a_session.get(api_url + url + '&timestamp')
        if len(w_a_response.json().get('archived_snapshots')) != 0:
            if w_a_response.json().get('archived_snapshots').get('closest').get('available'):
                arch_url_status = w_a_response.json().get('archived_snapshots').get('closest').get('status')
                arch_url = w_a_response.json().get('archived_snapshots').get('closest').get('url')
                raw_date = w_a_response.json().get('archived_snapshots').get('closest').get('timestamp')
                arch_date_formatted = " ".join([raw_date[6:8], months.get(raw_date[4:6]), raw_date[:4]])
                if arch_date_formatted[0] == "0":
                    arch_date_formatted = arch_date_formatted[1:]
                return arch_url, arch_date_formatted, arch_url_status
    except AttributeError:
        print("Unexpected error.")
    except ConnectionError:
        print("Connection to WebArchive failed.")
